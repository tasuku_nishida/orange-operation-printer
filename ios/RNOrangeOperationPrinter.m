
#import <Foundation/Foundation.h>
#import <React/RCTBridgeModule.h>

@interface RCT_EXTERN_MODULE(RNOrangeOperationPrinter, NSObject)

RCT_EXTERN_METHOD(setPrinterProperty:(NSDictionary *)printerPropertyDic)
RCT_EXTERN_METHOD(printReceipt:(NSString *)jsonString openDrawer:(BOOL)openDrawer callback:(RCTResponseSenderBlock)callback)

@end
