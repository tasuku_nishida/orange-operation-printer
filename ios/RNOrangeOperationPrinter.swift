

import Foundation

@objc(RNOrangeOperationPrinter)
class RNOrangeOperationPrinter: RCTEventEmitter {
    
    var printerProtocol: PrinterProtocol?
    var printerResultCode: PrinterResultCode?
    
    override static func requiresMainQueueSetup() -> Bool {
        return true;
    }
    
    override func supportedEvents() -> [String]! {
        return["Error", "PrintCompleted"]
    }
    
    @objc(setPrinterProperty:)
    func setPrinterProperty(printerPropertyDic: NSDictionary) {
        var _printerPropertyDic = printerPropertyDic as! [String : Any]
        _printerPropertyDic["BRIDGE"] = self
        let printerProperty = PrinterProperty(dict: _printerPropertyDic)
        
        switch printerProperty.printerSdk {
            case PrinterSDK.EPSON:
                printerProtocol = PrinterEpson(property: printerProperty)
            default: break
        }
    }
    
    @objc(printReceipt:openDrawer:callback:)
    func printReceipt(jsonString: NSString, openDrawer: Bool, callback: @escaping RCTResponseSenderBlock) {
        var result = printerProtocol?.startCreateReceipt()
        if !PrinterResult.isSuccess(result: result!) {
            callback([NSNull(), PrinterResult.printerResultToCallbackObj(result: result!)])
            return
        }
        
        result = printReceiptFromJson(jsonString: jsonString as String, openDrawer: openDrawer)
        if !PrinterResult.isSuccess(result: result!) {
            callback([NSNull(), PrinterResult.printerResultToCallbackObj(result: result!)])
            return
            
        }
        printerProtocol?.endCreateReceiptOut(completion: { (result) -> Void in
            callback([NSNull(), PrinterResult.printerResultToCallbackObj(result: result)])
        })
    }
    
    func printReceiptFromJson(jsonString: String, openDrawer: Bool) -> PrinterResultCode {
        let jsonData: Data = jsonString.data(using: String.Encoding.utf8)!
        do {
            let json = try JSONSerialization.jsonObject(with: jsonData, options: .allowFragments)
            let jsonArray = json as! [String : Any]
            let contents = jsonArray[ReceiptKey.CONTENT.rawValue] as! Array<Dictionary<String, NSObject>>
            
            let option = ReceiptOption(input: jsonArray)
            printerProtocol?.setReceiptOption(option: option)
            var drawerFlag = true
            
            for content in contents {
                var result: PrinterResultCode = PrinterResultCode.SUCCESS
                if(drawerFlag && openDrawer) {
                    result = (printerProtocol?.openDrawer())!
                    drawerFlag = false
                }
                switch (content[ReceiptKey.TYPE.rawValue] as! String) {
                case ReceiptElement.TEXT.rawValue:
                    let receiptText = ReceiptTypeText(dict: content)
                    result = (printerProtocol?.addText(receiptText: receiptText))!
                case ReceiptElement.BOTH.rawValue:
                    let receiptCombine = ReceiptTypeCombine(dict: content)
                    result = (printerProtocol?.addCombineText(receiptCombine: receiptCombine))!
                case ReceiptElement.IMAGE.rawValue:
                    let receiptImage = ReceiptTypeImage(dict: content)
                    result = (printerProtocol?.addImage(receiptImage: receiptImage))!
                case ReceiptElement.CUT.rawValue:
                    result = (printerProtocol?.cut())!
                case ReceiptElement.FEED.rawValue:
                    let receiptFeed = ReceiptTypeFeed(dict: content)
                    result = (printerProtocol?.feed(receiptFeed: receiptFeed))!
                case ReceiptElement.BARCODE.rawValue:
                    let receiptBarcode = ReceiptTypeBarcode(dict: content)
                    result = (printerProtocol?.barcode(receiptBarcode: receiptBarcode))!
                default:
                    break
                }
                if !PrinterResult.isSuccess(result: result) {
                    return result
                }
            }
            return PrinterResultCode.SUCCESS
        } catch {
            return PrinterResultCode.ERR_CODE_BEHAVIOR
        }
    }
    
    @objc(sendError:)
    func sendError(result: Int) {
        self.sendEvent(withName: "Error", body: PrinterResult.printerResultToCallbackObj(result: PrinterResultCode(rawValue: result)!))
    }
    
    @objc(sendPrintCompleted:)
    func sendPrintCompleted(result: Bool) {
        self.sendEvent(withName: "PrintCompleted", body: result)
    }
}
