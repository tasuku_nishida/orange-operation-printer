
# react-native-orange-operation-printer

## Getting started

`$ npm install react-native-orange-operation-printer --save`

### Mostly automatic installation

`$ react-native link react-native-orange-operation-printer`

### Manual installation


#### iOS

1. In XCode, in the project navigator, right click `Libraries` ➜ `Add Files to [your project's name]`
2. Go to `node_modules` ➜ `react-native-orange-operation-printer` and add `RNOrangeOperationPrinter.xcodeproj`
3. In XCode, in the project navigator, select your project. Add `libRNOrangeOperationPrinter.a` to your project's `Build Phases` ➜ `Link Binary With Libraries`
4. Run your project (`Cmd+R`)<

#### Android

1. Open up `android/app/src/main/java/[...]/MainActivity.java`
  - Add `import com.reactlibrary.RNOrangeOperationPrinterPackage;` to the imports at the top of the file
  - Add `new RNOrangeOperationPrinterPackage()` to the list returned by the `getPackages()` method
2. Append the following lines to `android/settings.gradle`:
  	```
  	include ':react-native-orange-operation-printer'
  	project(':react-native-orange-operation-printer').projectDir = new File(rootProject.projectDir, 	'../node_modules/react-native-orange-operation-printer/android')
  	```
3. Insert the following lines inside the dependencies block in `android/app/build.gradle`:
  	```
      compile project(':react-native-orange-operation-printer')
  	```

#### Windows
[Read it! :D](https://github.com/ReactWindows/react-native)

1. In Visual Studio add the `RNOrangeOperationPrinter.sln` in `node_modules/react-native-orange-operation-printer/windows/RNOrangeOperationPrinter.sln` folder to their solution, reference from their app.
2. Open up your `MainPage.cs` app
  - Add `using Orange.Operation.Printer.RNOrangeOperationPrinter;` to the usings at the top of the file
  - Add `new RNOrangeOperationPrinterPackage()` to the `List<IReactPackage>` returned by the `Packages` method


## Usage
```javascript
import RNOrangeOperationPrinter from 'react-native-orange-operation-printer';

// TODO: What to do with the module?
RNOrangeOperationPrinter;
```
  